import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

class ExelReader{

	public void readExel() throws BiffException, IOException{
		Map<Integer,String> mymap = new HashMap<>();
		
		
		
		String Filepath=("C:\\Users\\Andrew Gunasekara\\Desktop\\ExelReader\\PROD04-12_1133.xls");
		FileInputStream fs= new FileInputStream(Filepath);
		Workbook wb=Workbook.getWorkbook(fs);
		
		Sheet sh=wb.getSheet("PROD04-12_1133.xls");
		
		int TotalNoOfRows=sh.getRows();
		int TotalNoOfColumns=sh.getColumns();
		for (int row = 0; row < TotalNoOfRows; row++) {
			for (int col = 0; col <TotalNoOfColumns; col++) {
				System.out.print(sh.getCell(col, row).getContents() + "\t");
			}
			System.out.println();
		}
		
	}

	public static void main(String args[]) throws BiffException, IOException{
		ExelReader DT =new ExelReader();
		DT.readExel();
	
}
}